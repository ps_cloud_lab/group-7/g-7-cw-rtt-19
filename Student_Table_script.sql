\echo 'Dropping Tables Student_Marks, Subject_master, Student';
drop table if exists Student_Marks cascade;
drop table if exists Student cascade;
drop table if exists Subject_master cascade;

\echo 'Creating Tables'
Create table Student
(
Student_ID 		int,
Student_name 	varchar(30),
Student_Class 			int,
constraint pk_Stu_name primary key (Student_ID)
);

Create Table Subject_Master
(
Subject_ID 		int primary key,
Subject_name 	varchar(20) not null
);

Create Table Student_marks
(
Student_ID 		int,
Marking_period int not null,
Subject_ID 		int not null,
Marks			real not null,
Year Date 		not null default current_date,
constraint fk_Stu_Id foreign key (Student_ID) references Student(Student_ID),
constraint fk_Subj_Id foreign key (Subject_ID) references Subject_Master(Subject_ID),
constraint ck_mark check (Marks >=0 and marks <=100)
);

\echo 'Inserting into Student Table'

insert into student values (1, 'Sountharia', 9);
insert into student values (2, 'Chandra', 9);
insert into student values 
(3, 'Ammar', 9),
(4, 'Shamel', 9);

\echo 'Inserting into Subject Table'

insert into subject_master values(1,'Science'),(2,'Math'), (3, 'Social Studies'), (4, 'English'), (5, 'Physical Education');

\echo 'Inserting into Student Table'

insert into student_marks (Student_ID, Marking_period, Subject_ID,Marks) values 
(1, 1, 1, 80),(1, 1, 2,85),(1, 1, 3, 50),(1, 1, 4, 57.9),(1, 1, 5, 48),
(2, 1, 1, 70),(2, 1, 2, 90.5),(2, 1, 3, 90.7),(2, 1, 4, 90),(2, 1, 5, 78),
(3, 1, 1, 90),(3, 1, 2, 72),(3, 1, 3, 96),(3, 1, 4, 57.9),(3, 1, 5, 80),
(4, 1, 1, 100),(4, 1, 2, 88),(4, 1, 3, 50.6),(4, 1, 4, 68),(4, 1, 5, 98);


