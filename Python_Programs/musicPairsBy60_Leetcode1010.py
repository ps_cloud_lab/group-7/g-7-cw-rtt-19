class Solution:
    # def numPairsDivisibleBy60(self, time: List[int]) -> int:
    #     sum=0
    #     print(len(time))
    #     for i in range(len(time)):
    #         for j in range(i+1,len(time)):
    #             #print("i j :",time[i]+ time[j])
    #             if (time[i]+time[j])%60==0:
    #                 sum+=1
    #     return sum
    # class Solution:
    

    #def numPairsDivisibleBy60(self, time: list[int]) -> int:
    #    ans = 0
    #    count = [0] * 5
    #    print(count, time)
    #    for t in time:
    #        t %= 5
    #        print("t",t)
    #        ans += count[(5 - t) % 5]   
    #        print("ans=",ans,"count[(5 - ",t,") % 5] : ", "count[",(5 - t) % 5,"]: ",count[(5 - t) % 5], "count : ", count )
    #        # print(count)
    #        count[t] += 1
    #        print("Count[t], count : ",count[t],count)
    #    return ans

    def musicPairs(self,list:list[int]):
        count=[0]*5                #[0,0,0,0,0] Initializing the array with size 5(as it would not exceed the number becoz of MOD operation)
        ans=0
        for i in list:
            i%=5                    #i=2%5=2        i=3%5=3         i=2%5=2             i=2%5=2
            ans+=count[(5-i)%5]     #ans=0+c[3]=0+0 ans=0+c[2]=0+1  ans=1+c[3]=1+1=2    ans=1+c[3]=2+1=3
            count[i]+=1             #[0,0,1,0,0]    [0,0,1,1,0]     [0,0,2,1,0]         [0,0,3,1,0]
        return ans
#listNum=[5,5,5,5,5]
listNum=[2,3,2,2]
a=Solution()
#print(a.numPairsDivisibleBy60(listNum))
print(a.musicPairs(listNum))
