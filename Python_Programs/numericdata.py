print("Python has three numeric types: int, float, and complex")

myValue=6.7
print(myValue)
print(type(myValue))
print(str(myValue) + " is of the data type " + str(type(myValue)))

myValue1=5j
print(myValue1)
print(type(myValue1))
print(str(myValue1) + " is of the data type " + str(type(myValue1)))

myValue2=True
print(myValue2)
print(type(myValue2))
print(str(myValue2) + " is of the data type " + str(type(myValue2)))

myValue3=False
print(myValue3)
print(type(myValue3))
print(str(myValue3) + " is of the data type " + str(type(myValue3)))

myValue4=-8
print(myValue4)
print(type(myValue4))
print(str(myValue4) + " is of the data type " + str(type(myValue4)))

myValue5=8
print(myValue5)
print(type(myValue5))
print(str(myValue5) + " is of the data type " + str(type(myValue5)))

myValue6=-6.7
print(myValue6)
print(type(myValue6))
print(str(myValue6) + " is of the data type " + str(type(myValue6)))

myValue7=898987654456899078899
print(myValue7)
print(type(myValue7))
print(str(myValue7) + " is of the data type " + str(type(myValue7)))

myValue8="true"
print(myValue8)
print(type(myValue8))
print(str(myValue8) + " is of the data type " + str(type(myValue8)))

myValue9="hello"
print(myValue9)
print(type(myValue9))
print(str(myValue9) + " is of the data type " + str(type(myValue9)))



