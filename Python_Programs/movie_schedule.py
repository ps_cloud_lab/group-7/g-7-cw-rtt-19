current_movies = { 'Moana':'10.00am', 
                    'Frozen2':'12.30pm', 
                    'Pirate Fairy':'3.00pm',
                    'Secret of the Wings':'5pm',
                    'Tangled':'7pm'}   
                                     
#current_movies={}
#current_movies['Moana'] = '10.00am'
print("We're showing the following movies:")
for key in current_movies:
    print(key)
movie = input('What movie would you like the showtime for?\n')                  

showtime = current_movies.get(movie)
if showtime == None:
    print("Requested movie or showtime isn't playing")
else:
    print(movie, 'is playing at', showtime)