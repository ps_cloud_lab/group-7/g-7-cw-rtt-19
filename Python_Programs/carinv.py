import csv
# https://docs.python.org/3/library/csv.html?highlight=import%20csv
# https://www.programiz.com/python-programming/shallow-deep-copy
import copy
#https://www.tutorialsteacher.com/python/os-module
import os
# https://python-course.eu/applications-python/sys-module.php#:~:text=the%20sys%20Module-,Information%20on%20the%20Python%20Interpreter,with%20the%20import%20statement%2C%20i.e.&text=The%20sys%20module%20provides%20information,available%20constants%2C%20functions%20and%20methods.
import sys

myVehicle = { "vin" : "<empty>", "make" : "<empty>", "model" : "<empty>", "year" : 0, 
                "range" : 0, "topSpeed" : 0, "zeroSixty" : 0.0, "mileage" : 0 }
for key, value in myVehicle.items():
    print("{} : {} : {}".format(key,value,"lekha"))

myInventoryList = []

#with open('/home/ec2-user/environment/lekhapython/car_fleet.csv') as csvFile:
#This can also be used if the .csv file is in the current directory

#with open(sys.path[0] + "/" + "car_fleet.csv") as csvFile: 

# https://www.tutorialspoint.com/How-to-open-a-file-in-the-same-directory-as-a-Python-script
with open(os.path.join(sys.path[0], "car_fleet.csv"), "r") as csvFile:

    csvReader = csv.reader(csvFile, delimiter=',')  
    lineCount = 0  
    for row in csvReader:
        if lineCount == 0:
            print(f'Column names are: {", ".join(row)}')  
            lineCount += 1  
        else:  
            print(f'vin: {row[0]}, make: {row[1]}, model: {row[2]}, year: {row[3]}, range: {row[4]}, topSpeed: {row[5]}, zeroSixty: {row[6]}, mileage: {row[7]}')  
            currentVehicle = copy.deepcopy(myVehicle)  
            currentVehicle["vin"] = row[0]  
            currentVehicle["make"] = row[1]  
            currentVehicle["model"] = row[2]  
            currentVehicle["year"] = row[3]  
            currentVehicle["range"] = row[4]  
            currentVehicle["topSpeed"] = row[5]  
            currentVehicle["zeroSixty"] = row[6]  
            currentVehicle["mileage"] = row[7]  
            myInventoryList.append(currentVehicle)  
            lineCount += 1  
    print(f'Processed {lineCount} lines.')
    for myCarProperties in myInventoryList:
        for key, value in myCarProperties.items():
            print("{} : {}".format(key,value))
            print("-----")
            
    
