#List types
myFruitList = ["apple", "banana", "cherry"]
print(myFruitList)
print(type(myFruitList))
print(myFruitList[0])
print(myFruitList[1])
print(myFruitList[2])
print(myFruitList[0],myFruitList[1],myFruitList[2])
myFruitList[2] = "orange"
print(myFruitList)
print(type(myFruitList))
myFinalAnswerTuple = ("pear", "kiwi", "pineapple")
print(myFinalAnswerTuple)
print(type(myFinalAnswerTuple))
print(myFinalAnswerTuple[0])
print(myFinalAnswerTuple[1])
print(myFinalAnswerTuple[2])
myFavoriteFruitDictionary = { "chandra" : "mango", "lekha" : "watermelon","chandralekha" : "jackfruit"}
print(myFavoriteFruitDictionary)
print(type(myFavoriteFruitDictionary))
print(myFavoriteFruitDictionary["chandra"])
print(myFavoriteFruitDictionary["lekha"])
print(myFavoriteFruitDictionary["chandralekha"])
print(myFavoriteFruitDictionary["Perscholas"])
