# Python program to find the
# maximum of two numbers
     
a = 2
b = 4
 
# Use of ternary operator
print(a if a >= b else b)