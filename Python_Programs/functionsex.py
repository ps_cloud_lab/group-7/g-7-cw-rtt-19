#example1
def demo(x):
        y=x+3
        return y

print(demo(1))


#example2
def demo1():
    d = (a+b+c)
    print(d)
    
a=10
b=10
c=10
demo1()

#example3
#value of pi
pi = 3.14
#calculate area of circle for given radius
def calculate_area_circle(radius):
    return pi*radius**2
    
r = int(input("Enter the radius of the circle: "))
area = calculate_area_circle(r)
print(area)  

#example4
def greet_user(): 
    print("Hello there!")
    
greet_user()

def greet_user1(name): 
    print("Hello " + name)
    
greet_user1("python")

#example5
import math
absolute = -5.999 
floor_test = 198.42
result1 = math.fabs(absolute) 
result2 = math.floor(floor_test)
print(result1, " is the absolute value of ", absolute) 
print(result2, " is the flow of ", floor_test)

