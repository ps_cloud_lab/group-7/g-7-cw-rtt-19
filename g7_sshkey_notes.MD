# SSH keypair creation

In this article we are going to focus on how to create an SSH key and use the keys to communicate with the Gitlab servers.


## Generating a SSH Key Pair
1. Open a terminal

2. Create a directory name .ssh
```
mkdir .ssh
```
![creating .ssh directory](/sshkey_images/create_dir.png "creating .ssh directory")

3. Change permission on .ssh directory
```
ls -ld .ssh
chmod 700 .ssh/
ls -ld .ssh
```

![change permission .ssh directory](/sshkey_images/change_perm.png "change permission .ssh directory")

4.  Create 4096 bit RSA keypair
```
cd .ssh
ssh-keygen -t rsa -b 4096 -C "<dev.lastname@gmail.com>"
ls -ld .ssh
```
![create ssh keypair](/sshkey_images/key_creation.png "change permission .ssh directory")

5. Create config file and change permission
```
touch config
ls -al
chmod 600 config
ls -al
```
![create config file](/sshkey_images/config_creation.png "create config file and change permission")

6. Change permission of private ssh key
```
ls -al
chmod 400 gitlabkey
ls -al
```
![change private key permission](/sshkey_images/sshprivatekey_change_perm.png "change private key permission")

7. copy the public key into the gitlab account
```
### open https://gitlab.com/-/profile/keys and add an sshkey(public) with the below output
cat gitlabkey.pub 
```
![copy public key to gitlab account](/sshkey_images/publicKey.png "copy public key to gitlab account")

8. Configure created ssh keypair into config file
```
vi config

### paste the below section 
# ChandraLekha Gitlab Account Identity
Host gitlab
  Hostname gitlab.com
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/gitlabkey

```
![create config file](/sshkey_images/config_file_creation.png "create config file and change permission")

9. Add private key to ssh-agent and verify keys
```
eval $(ssh-agent -s)
ssh-add ./gitlabkey
ssh git@gitlab.com -T
```
![add private key to ssh-agent](/sshkey_images/add_sshkey_agent.png "add private key to ssh-agent")



This completes the SSH Keypair generation along with successfully configuring it in ssh-agent for gitlab authentication.

Below is the updated config file

![create config file](/sshkey_images/config_file_pwdkeychain.png "create config file with fetching stored pwd from keychain")

## Managing custom Named SSH Key
Managing SSH keys can become cumbersome as soon as you need to use a second key. Traditionally, you would use ssh-add to store your keys to ssh-agent, typing in the password for each key. The problem is that you would need to do this every time you restart your computer, which can quickly become tedious.
A better solution is to automate adding keys,store passwords and to specify which key to use when accessing.
The first thing we are going to solve using this config file is to avoid having to add custom named SSH keys using ssh-add
Next, make sure that ~/.ssh/gitlabkey is not in ssh-agent by opening terminal and running the following command
```
ssh-add -D
```
This command will remove all keys from the currently active ssh-agent session.
Now if you try cloning a Gitlab repository, your config file will use the key at ~/.ssh/gitlabkey


## Password Management
It can get very tedious entering a password every time you initialize a SSH connection. To get around this, we can use the password management software that comes with macOS and various Linux distributions.
As I am working on Mac,I am using macOS's Keychain Access program. Start by adding your key to the Keychain Access by passing -K option to the ssh-add command
```
ssh-add -K ~/.ssh/gitlabkey
```
Now you can see your SSH key in Keychain Access.
But if you remove the keys from ssh-agent with ssh-add -D or restart your computer, you will be prompted for password again when you try to use ssh
Open your ssh config file by running 
```
vi ~/.ssh/config
```
 and add the following
 ```
 Host *
  AddKeysToAgent yes
  UseKeychain yes
  ```
  With that, whenever you run ssh it will look for keys in Keychain Access. If it finds one, you will no longer be prompted for a password. Keys will also automatically be added to ssh-agent every time you restart your machine.

  To automate the load of the private key to the terminal, we could make use of the profile. By default .zsh is the default terminal in mac.
I am using the created .zprofile in my home directory. 

![add private key to ssh-agent](/sshkey_images/home_dir.png "path to home directory")


![add private key to ssh-agent](/sshkey_images/zprofile_loca.png "Zprofile lacation")

Use 
```

vi .zprofile

```
to open the .zprofile and add the command

```

ssh -T Gitlab

```
here -T stands for "Disable pseudo-terminal allocation"

![add private key to ssh-agent](/sshkey_images/open_zprofile.png "open zprofile")

When the terminal starts, it loads the .zprofile and by executing ssh -T Gitlab command, it loads the private key and the password from the mac key-store, since we had specified those options in the config file which adds the private key to the ssh agent thereby making it available to us for further use of the key.

When you open your terminal, this will be your default 

![add private key to ssh-agent](/sshkey_images/FinalOutput.png "Final Output")




## Links

You may follow the below link which details all of the steps mentioned above along with other details and clear explanation [Gitlab SSH keypair](https://docs.gitlab.com/ee/ssh/).
