--\d <table name> -- to describe a table
--\password <user name>  -- \password postgres to change the password of a user

--to alter null to not null column
alter table Student alter column Student_name set Not null;
alter table Student alter column class set Not null;

--to alter datatype
alter table Student alter column Student_name type varchar(50);

-- to alter column name
alter table Student rename column class to Student_class;

-- average mark of student 1
select avg(marks) from Student_marks where student_id = 1;

-- round avg to 2 decimal places
select round(avg(marks)::numeric,2) from Student_marks where student_id = 1;

-- view all student details 
select a.student_id, 
		a.student_name, 
		a.Student_class, 
		b.subject_id, 
		c.subject_name, 
		round(marks::numeric,1) marks,
	    to_char(year,'yyyy') Student_year
  from student a, 
		student_marks b, 
		subject_master c 
 where a.student_id = b.student_id 
   and b.subject_id = c.subject_id
 order by b.Student_id, b.Subject_id;


-- group by clause to find the avg mark of each student
select a.student_id, 
		a.student_name, 
		a.Student_class, 
		round(avg(marks)::numeric,1) marks,
	    to_char(year,'yyyy') Student_year
  from student a, 
		student_marks b 
 where a.student_id = b.student_id 
 group by a.Student_Id, a.Student_name, a.Student_class,  to_char(year,'yyyy')
 order by a.Student_id;

-- group by clause and having clause to find the student who have avg mark > 75
select a.student_id, 
		a.student_name, 
		a.Student_class, 
		round(avg(marks)::numeric,1) marks,
	    to_char(year,'yyyy') Student_year
  from student a, 
		student_marks b 
 where a.student_id = b.student_id 
 group by a.Student_Id, a.Student_name, a.Student_class,  to_char(year,'yyyy')
 having round(avg(marks)::numeric,1) >= 75
 order by a.Student_id;



--select count of records for subject 1 of student 1 
select count(*)
  from Student_marks
 where Student_Id = 1
   and Subject_Id = 2;


--update mark of subject 1 for student 1 details
--returning displays the affected rows.
update Student_marks
   set marks = 94
 where Student_Id = 1
   and Subject_Id = 2
returning *; 
